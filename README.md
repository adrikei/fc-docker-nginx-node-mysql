Execute `$ docker-compose up`

Note: if you had the repo before and it's crashing on missing node dependencies, the only way I got it running was by doing a `$ docker system prune` before upping again. Still to figure it out.

Check `localhost:8080` on your browser.