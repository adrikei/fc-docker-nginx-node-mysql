const express = require("express");
const app = express();
const mysql = require("mysql2");
const _chance = require("chance");
const chance = new _chance();

const port = 3000;

const dbconfig = {
  host: "mysql",
  user: "root",
  password: "root",
  database: "nodedb",
};

const dbconn = mysql.createConnection(dbconfig);

const nameToAdd = chance.name();
const addNameQuery = `insert into people(name) values ('${nameToAdd}');`;
dbconn.execute(addNameQuery);

app.get("/", (req, res) => {
  dbconn.execute("select * from people", (err, rows) => {
    if (err) {
      res.send("<h1>Couldn't fetch names</h1>");
    } else {
      var arr = ["<h1>Full Cycle Rocks!</h1>"];
      for (row of rows) {
        // console.log(row);
        arr.push(`<h3>${row.id} - ${row.name}</h3>`);
      }
      res.send(arr.join(" "));
    }
  });
});

app.listen(port, () => {
  console.log("rodando na porta", port);
});
